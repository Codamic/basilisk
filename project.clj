(defproject codamic/basilisk "0.1.0-SNAPSHOT"
  :description "Distributed Datalog Implementation."
  :license {:name "Apache License v2"
            :url "https://www.apache.org/licenses/LICENSE-2.0"}
  :url         "http://basilisk.io"
  :scm     {:name "git"
            :url "https://github.com/Codamic/basilisk"}
  :dependencies [[org.clojure/clojure  "1.10.1-beta2"]
                 [codamic/hellhound.core "1.0.0-SNAPSHOT"]
                 ;; Avro
                 [org.apache.avro/avro "1.8.2"]
                 ;; Zookeeper
                 [org.clojure/core.rrb-vector "0.0.14"]]

  :plugins [[lein-sub "0.3.0"]
            [lein-codox "0.10.3"]
            [lein-kibit "0.1.6"]
            [jonase/eastwood "0.2.7"]
            [lein-bikeshed "0.5.1"]]

  :resource-paths ["resources"]
  :source-paths ["src/clj"]

  :uberjar-name "basilisk.standalone.jar"
  :jar-name "basilisk.jar"

  :codox {:output-path "docs/api/"
          :metadata {:doc/format :markdown}
          :doc-paths ["docs/guides/"]
          :source-uri "http://github.com/Codamic/basilisk/blob/{version}/{filepath}#L{line}"
          :source-paths ["core/src"]}

  :main ^:skip-aot basilisk.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :dev {:dependencies [[org.clojure/tools.namespace "0.2.11"]]
                   :source-paths ["dev"]
                   :env {}}})
