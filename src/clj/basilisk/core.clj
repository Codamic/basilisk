(ns basilisk.core
  (:require
   [hellhound.logger :as logger]
   [hellhound.system :as sys]
   [basilisk.systems :as systems]))


(defn -main [& args]
  (.addShutdownHook
   (Runtime/getRuntime)
   (Thread. (fn []
              (logger/info "Shutting down...")
              (sys/stop!))))

  (sys/set-system! systems/transactor-system)
  (sys/start!))
