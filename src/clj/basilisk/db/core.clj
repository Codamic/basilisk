;; Copyright <2019> <Sameer Rahmani>

;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at

;;     http://www.apache.org/licenses/LICENSE-2.0

;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

;; Some of the code in this file borrowed from hitchhiker-tree imlementation
;; distributed under EPL at https://github.com/dgrnbrg/hitchhiker-tree
(ns basilisk.db.core
  (:require
   [basilisk.serializer.protocols :as sr]
   [basilisk.storage :as st]
   [basilisk.coordination :as cr]
   [basilisk.db.impl.db :as impl]
   [basilisk.db.protocols :as proto]
   [basilisk.db.transactions :as tx]))

(defn db? [x]
  (and ;;(satisfies? proto/ISearch x)
       ;;(satisfies? proto/IIndexAccess x)
       (satisfies? proto/IDB x)))

(defn connection?
  "Returns `true` if this is a connection to a basilisk db, `false` otherwise."
  [conn]
  (and (instance? clojure.lang.IDeref conn)
       (db? @conn)))

(defn with
  "Same as [[transact!]], but applies to an immutable database value. Returns transaction report (see [[transact!]])."
  ([db tx-data] (with db tx-data nil))
  ([db tx-data tx-meta
    {:pre [(db? db)]}
    (if (is-filtered db)
      (throw (ex-info "Filtered DB cannot be modified" {:error :transaction/filtered}))
      (tx/transact-tx-data (tx/map->TxReport
                            {:db-before db
                             :db-after  db
                             :tx-data   []
                             :tempids   {}
                             :tx-meta   tx-meta}) tx-data))]))

(defn transact!
  [conn tx-data tx-meta]
  {:pre [(connection? conn)]}
  (let [report (atom nil)]
    (swap! conn (fn [db]
                  (let [r (with db tx-data tx-meta)]
                    (reset! report r)
                    (:db-after r))))
    @report))

(defn make-dummy-db
  []
  (impl/make-db (st/make-dummy-storage)
                (cr/make-dummy-coordinator)
                {}))

(comment
  (db? (make-dummy-db)))


;; TYPES & TYPES HELPERS ------------------------------------------------------
(defrecord Datum [entity attribute value transaction add-retract]
  sr/Serializable
  (serialize [datum]
    (concat (str entity)
            "___"
            (str attribute)
            "___"
            (str value)
            "___"
            (str transaction)
            "____"
            (str add-retract))))


(defn ->datum
  [& [entity attribute value transaction add-retract]]
  (->Datum entity attribute value transaction add-retract))


;; INSTRUCTIONS ---------------------------------------------------------------
(defn insert
  [db & datum-args]
  {:pre (db? db)}
  (let [datum (apply ->datum datum-args)]
    (proto/insert db datum)))

(defn execute-instruction
  [db [instruction & args]]
  (cond
    (= instruction :basilisk.db/insert) (apply insert db args)
    :else nil))


(comment
  (def db (make-dummy-db))
  (execute-instruction db [:basilisk.db/insert "sam" :has "book" 23 true])
  (execute-instruction db [:basilisk.db/insert "sam1" :has "basdook" 23 true])
  @(.memory (:storage db)))
