;; Copyright <2019> <Sameer Rahmani>

;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at

;;     http://www.apache.org/licenses/LICENSE-2.0

;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns basilisk.trees.core)


(defn dummy-cmp
  [pair-a pair-b]
  (< (key pair-a) (key pair-b)))


(defn slice
  [node k]
  (seq (filter #(< k (key %)) node)))

(def a (sort
        (Node. [(pair 4 1) (pair 1 2) (pair 9 11)
                (pair 2 3) (pair 7 3) (pair 4 2)] dummy-cmp 16)))
(count (.pairs a))
(count a)
(seq a)
(rest a)
(next a)
(insert a (pair 99 88))
(insert a (pair 0 33))
(type (sort a))
(find (sort a) 4)
(map (fn [p] (println (second p))) a)

(deftype BPTree [storage cmp-fn]
  Insert
  (insert [tree datum]
    (loop [])))

(defn create
  [cmp-fn]
  (BPTree. cmp-fn))

(class '(1))
(class (Example. '(1 2 3 4)))
(class (first (filter odd? (Example. '(1 2 3 4)))))
