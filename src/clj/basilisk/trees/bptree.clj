;; Copyright <2019> <Sameer Rahmani>

;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at

;;     http://www.apache.org/licenses/LICENSE-2.0

;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

;; Some of the code in this file borrowed from hitchhiker-tree imlementation
;; distributed under EPL at https://github.com/dgrnbrg/hitchhiker-tree
(ns basilisk.trees.bptree
  (:refer-clojure :exclude [compare resolve subvec])
  (:require
   [clojure.core.rrb-vector :refer [catvec subvec]]
   [basilisk.types.protocols :as tp]
   [basilisk.trees.protocols :as proto])

  (:import
   [java.util Collections]))


(defrecord Split [left-node right-node median])

(defrecord IndexNode [config pairs]
  proto/Resolve
  (index? [_] true)

  (dirty? [node] (or (::dirty? node) false))

  (last-key [node] (:key (peek pairs)))
  (resolve! [node]
    ;; TODO: Look into `config` and use the page manager to resolve this node.
    (delay node))

  proto/Node
  (overflow? [node]
    (>= (count pairs) (:branching-factore config)))

  (underflow? [node]
    (< (count pairs) (/ (:branching-factore config) 2)))

  (merge-node [node other-node]
    (->IndexNode config
                 (catvec pairs (:pairs other-node))))

  (split-node [node]
    (let [b (:branching-factore config)
          median (:key ^proto/Pairable (nth pairs (dec (/ b 2))))]

      (->Split (->IndexNode config (subvec pairs 0 b))
               (->IndexNode config (subvec pairs b))
               median)))

  (lookup [root-node k]
    ;;This is written like so because it's performance critical
    (let [keys (map :key pairs)
          ;; TODO: do we need to get the keys ? can't we get the key
          ;; in the comparator ???
          result (Collections/binarySearch keys k tp/compare)]
      (if (neg? result)
        (- (inc result))
        result))))

(comment
  (def a (->IndexNode {:branching-factore 3} [{:key 3} {:key 5} {:key 1}]))
  (proto/split-node a))
