;; Copyright <2019> <Sameer Rahmani>

;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at

;;     http://www.apache.org/licenses/LICENSE-2.0

;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

;; Some of the code in this file borrowed from hitchhiker-tree imlementation
;; distributed under EPL at https://github.com/dgrnbrg/hitchhiker-tree
(ns basilisk.trees.protocols
  (:refer-clojure :exclude [compare resolve subvec]))


(defprotocol Resolve
  "All nodes must implement this protocol. It's includes the minimal functionality
   necessary to avoid resolving nodes unless strictly necessary."
  (^Boolean index? [_]
    "Returns true if this is an index node")
  (last-key [_]
    "Returns the rightmost key of the node")
  (^Boolean dirty? [_]
    "Returns true if this should be flushed")
  ;;TODO resolve should be instrumented
  (^Node resolve! [_]
    "Returns a Node type of this node could, trigger IO"))

(defprotocol Node
  (^Boolean overflow? [node]
    "Returns true if this node has too many elements")
  (^Boolean underflow? [node]
    "Returns true if this node has too few elements")
  (^Node merge-node [node other]
    "Combines this node with the other to form a bigger node. We assume they're siblings")
  (split-node [node]
    "Returns a Split object with the 2 nodes that we turned this into")
  (lookup [node key]
    "Returns the child node which contains the given key"))
