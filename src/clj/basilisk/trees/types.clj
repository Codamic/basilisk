(ns basilisk.trees.types
  (:require
   [basilisk.bptree.protocols :as proto])
  (:import
   [java.util Collections]))

(defrecord Datum [entitry attribute value transaction add-retract])


(deftype ListPair [key_ value_]
  proto/Pairable
  (key [pair]
    (.key_ pair))

  (value [pair]
    (.value_ pair)))


(defmethod print-method ListPair
  [v ^java.io.Writer w]
  (.write w (str "<" (proto/key v) ":" (proto/value v) ">")))

(defn pair
  [key value]
  (ListPair. key value))


(deftype Node [pairs cmp-fn fan-out-size]
  proto/TreeNode
  (insert [node pairable-value]
    (when (< (count (.pairs node)) fan-out-size)
      (sort
       (Node. (conj (.pairs node) pairable-value)
              cmp-fn
              fan-out-size))))

  (find [node k]
    (let [result (Collections/binarySearch (.pairs node)
                                           (pair k nil)
                                           (clojure.core/comparator
                                            (fn [x y]
                                              (< (key x) (key y)))))]
      (when (< -1 result)
        (nth (.pairs node) result))))

  proto/Sortable
  (sort-fn [node]
    cmp-fn)

  (sort [node]
    (Node.
     (clojure.core/sort (sort-fn node) (seq node))
     cmp-fn
     fan-out-size))

  clojure.lang.Sequential
  clojure.lang.IPersistentCollection
  (count [node]
    (count (.pairs node)))

  (cons [node pair]
    (Node.
     pairs
     cmp-fn
     fan-out-size))

  (empty [node]
    (Node. '() cmp-fn fan-out-size))

  (equiv [node o]
    true)

  (seq
    [node] (seq (.pairs node)))

  clojure.lang.Sorted
  (seq [node ascending?] (seq (.pairs node)))

  (comparator [node]
    (clojure.core/comparator cmp-fn))

  (seqFrom [node key ascending?]
    ;; ascending? is useless here, Nodes are always sorted in ascending.
    node)

  (entryKey [node entry]
    (key entry))

  clojure.lang.ISeq
  (first [node]
    (first (.pairs node)))

  (next [node]
    (next (.pairs node)))

  (more [node]
    (rest (.pairs node))))
