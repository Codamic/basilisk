;; Copyright <2019> <Sameer Rahmani>

;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at

;;     http://www.apache.org/licenses/LICENSE-2.0

;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

;; Some of the code in this file borrowed from hitchhiker-tree imlementation
;; distributed under EPL at https://github.com/dgrnbrg/hitchhiker-tree

(ns basilisk.coordination.impl.dummy
  (:require
   [basilisk.coordination.protocols :as proto]))

(deftype DummyCoordinator [memory]
  proto/IAtomicCounter
  (get-value
   [counter]
   @(.memory counter))

  (inc
   [counter
    (swap! (.memory counter) inc)]))

(defn make-coordinator
  []
  (DummyCoordination. (atom 0)))

(comment
  (def a (make-dummy-coordinator))
  (proto/get-value a)
  (proto/inc a))
